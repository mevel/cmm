open Cparse
(*open Genlab*)


(** Output utilities (printf-like). **)

(* Writing assembly code on the output file. *)
let global_out = ref stdout
let print fmt = Printf.kfprintf (fun f -> output_char f '\n') !global_out fmt

(* Delayed writing of assembly code. *)
let pending_buf = Buffer.create 1024
let pending fmt = Printf.kbprintf (fun b -> Buffer.add_char b '\n') pending_buf fmt
let print_pending () = Buffer.output_buffer !global_out pending_buf

(* Errors and warnings. *)
let warning loc fmt = Printf.ksprintf (Error.warning loc) fmt
let error   loc fmt = Printf.ksprintf (Error.error   loc) fmt


(** Useful stuffs. **)

module Util = struct
  let rec list_drop n l =
    if n = 0 then l else list_drop (n-1) (List.tl l)
  let rec list_zip_truncate l m = match l, m with
    | [], _
    | _, []        -> []
    | a::l', b::m' -> (a,b) :: list_zip_truncate l' m'
end


(** Don’t worry, this won’t occur. **)

exception Not_implemented of string
exception Wont_happen


(** Configuration of the compiler (mainly intended to ease switching between
 ** 32-bit and 64-bit architecture). **)

  (* TODO: find an elegant way to parametrize the mnemonic’s suffix (l or q),
     register names and all that stuff… *)

module Config = struct
  (* print some debug messages: *)
  let debug = false
  (* size, in bytes, of a machine word: *)
  let word = 8
  (* registers used by the calling convention for integer arguments, in order: *)
  let call_registers = ["%rdi"; "%rsi"; "%rdx"; "%rcx"; "%r8"; "%r9"]
  (* registers which are guaranteed not to be altered after a function returns: *)
  let callee_saved_registers = [(*"%rsp"; "%rbp";*) "%rbx"; "%r12"; "%r15"]
  (* registers freely available for computations: *)
  let free_registers = ["%rax"; "%r10"; "%r11"; "%r13"; "%r14";
                        "%r9"; "%r8"; "%rcx"; "%rdx"; "%rsi"; "%rdi"]
  (* usual registers (reg0 is the main register and the one used for return values): *)
  let reg0 :: reg1 :: reg2 :: _ = free_registers
end


(** Representation of an environment, that is, the set of known objects
 ** in the current scope and their location in memory). **)

(* where and how an object is stored: *)
type obj_storage =
  | Static
  | Stack of int          (* stack offset (in words) *)
  | Register of string    (* register name *)
(* the type of an object (C-- only comprises int variables and functions): *)
type obj_type =
  | Var
  | Func of int           (* number of parameters *)
(* an object: *)
type obj =
  { name:    string;
    otype:   obj_type;
    storage: obj_storage }
(* an environment: *)
type env =
  { objs:    obj list;        (* known objects *)
    off:     int;             (* current stack offset (in words) *)
    finally: string option }  (* label where to jump instead of returning immediately *)

(* Newly declared objects are added to the head of the environment. When
 * looking for an object by its name, we select the first object with that
 * name appearing in the list, that is the last declared one. A local variable
 * temporarily hides the objects of the same name declared in a containing scope.
 *)

let object_exists objs name =
  List.exists (fun o -> o.name = name) objs
let find_object objs name =
  List.find (fun o -> o.name = name) objs


(** Exceptions. **)

let exceptions = Hashtbl.create 256
let exception_count = ref (-1)

(* Return a unique integral code associated with an exception’s name; create
 * that code if necessary. *)
let exception_code name =
  try
    Hashtbl.find exceptions name
  with Not_found ->
    incr exception_count;
    Hashtbl.add exceptions name !exception_count;
    !exception_count

(* Some constants to handle special cases in the finally statement. *)
let exccode_nothing = exception_code ""            (* “nothing to do” *)
and exccode_return  = exception_code "<return>"    (* “a return to be propagated” *)

(* Produce an array containing all exceptions’ names indexed by their codes. *)
let label_excnames = ".excnames"
let compile_exceptions () =
    pending "";
    pending ".section .data";
    (* Produce a separate string for each exception name. *)
    Hashtbl.iter
     (fun name code ->
       let label = Printf.sprintf "%s_%u" label_excnames code in
       pending "\t.hidden\t%s" label;
       pending "\t.type\t%s, @object" label;
       pending "\t.align\t4";
       pending "\t.size\t%s, %u" label (String.length name + 1);
       pending "  %s:" label;
       pending "\t.string \"%s\"" (String.escaped name);
     )
     exceptions;
    (* Generate the array containing all these strings. *)
    pending "";
    pending "\t.local\t%s" label_excnames;
    pending "\t.type\t%s, @object" label_excnames;
    pending "\t.align\t4";
    pending "\t.size\t%s, %u" label_excnames (Config.word * !exception_count);
    pending "  %s:" label_excnames;
    for i = 0 to !exception_count do
      pending "\t.%ubyte\t%s_%u" Config.word label_excnames i
    done


(* In order to print error messages for uncaught exceptions leading the program
 * to terminate, we need to catch them. We do this by wrapping the function
 * ‘main’ in a “try…finally” statement; the true function ‘main’ contains this
 * statement and a call to the user’s function (which is renamed according to
 * the ‘label_main’ constant).
 * We cannot simply add the desired code to the user’s function because it might
 * be called recursively.
 *)

let label_main = ".main"
let function_name name =
  if name = "main" then label_main else name


(** The compilation functions. **)

(* Conventions:
 *   Function calls follow the GCC convention for 64-bit architecture:
 *   — first six (integer) arguments in %rdi, %rsi, %rdx, %rcx, %r8, %r9,
 *     the remaining in the stack after the return address (7th argument
 *     being the leftmost);
 *   — each function must ensure that %rsp, %rbp, %r12, %r15 will not be
 *     altered after their return;
 *   — the result value is found in %rax.
 *   Additionally, for the sake of simplicity, the evaluation of an expression
 *   (and sub-expression) also satisfies the last two constraints.
 *   Moreover:
 *   — when in a “try” statement, %rbp contains the value which has to be
 *     restored in %rsp (the stack pointer) if a value is returned or an
 *     exception is thrown; 0(%rbp) is the previous value of %rbp and -8(%rbp)
 *     is the instruction address to jump to if an exception is thrown;
 *   — thrown exceptions have their code in ‘Config.reg0’ and their value in
 *     ‘Config.reg1’.
 *)

(* The main compilation function. *)
let rec compile out decl_list =
  global_out := out;
  compile_main ();
  let env0 = {objs = []; off = 0; finally = None} in
  compile_decl_list true env0 decl_list |> ignore;
  compile_exceptions ();
  print_pending ()

(* Compile the ‘main’ wrapper handling uncaught exceptions. *)
and compile_main () =
  let label_finally = ".main_finally"
  and label_fmt     = ".main_fmt" in
  print ".section .text";
  print "\t.globl\tmain";
  print "\t.type\tmain, @function";
  print "  main:";
  (* “Try” header. *)
  print "\tpushq\t%%rbp";
  print "\tmovq\t%%rsp, %%rbp";
  print "\tpushq\t$%s" label_finally;
  (* Call to the user’s ‘main’ function. *)
  print "\tcallq\t%s" label_main;
  (* Normal ending. *)
  free 1;
  print "\tpopq\t%%rbp";
  print "\tretq";
  (* “Finally” clause catching any uncaught exception and printing a message
     before ending. *)
  print "  %s:" label_finally;
  let arg1 :: arg2 :: arg3 :: arg4 :: _ = Config.call_registers in
  print "\tmovq\tstderr, %s" arg1;
  print "\tmovq\t$%s, %s" label_fmt arg2;
  print "\tmovq\t$%s, %s" label_excnames arg3;
  print "\tmovq\t(%s,%s,%u), %s" arg3 Config.reg1 Config.word arg3;
  print "\tmovq\t%s, %s" Config.reg0 arg4;
  print "\tmovq\t$0, %s" Config.reg0;
  print "\tcallq\tfprintf";
  print "\tpopq\t%%rbp";
  (* End the program with return code -1 to signal an error. *)
  print "\tmovq\t$-1, %s" Config.reg0;
  print "\tretq";
  (* Store the format string used. *)
  print "";
  let value = "Uncaught exception: %s(%i)\n" in
  print ".section .data";
  print "\t.local\t%s" label_fmt;
  print "\t.type\t%s, @object" label_fmt;
  print "\t.align\t4";
  print "\t.size\t%s, %u" label_fmt (String.length value + 1);
  print "  %s:" label_fmt;
  print "\t.string \"%s\"" (String.escaped value);

(* Compile a list of declarations, whether global or local.
   Return the resulting environment. *)
and compile_decl_list toplevel (*env decl_list*) =
  List.fold_left (compile_decl toplevel) (*env decl_list*)

(* Add the given list of function parameters to the environment, without
   actually producing the code to allocate space on the stack (only computes
   the stack offset of each variable and the resulting new position of the
   stack pointer).
   Return the resulting environment. *)
and compile_param_list env param_list =
  compile_decl_list false env (List.rev param_list)

(* Compile a single declaration, global or local.
   Return the resulting environment. *)
and compile_decl toplevel env = function
  (* global variable: add it to the context and allocate it in a data section *)
  | CDECL (loc,name) when toplevel ->
      if object_exists env.objs name then
        error (Some loc) "redefinition of global object ‘%s’" name;
      let obj = {name; otype = Var; storage = Static} in
      let env' = {env with objs = obj::env.objs} in
      (* Note: C-- does not support initialization of variables, thus we can
         put them in the BSS section. *)
      print "";
      print ".section .bss";
      print "\t.globl\t%s" name;
      print "\t.type\t%s, @object" name;
      print "\t.align\t4";
      print "\t.size\t%s, %u" name Config.word;
      print "  %s:" name;
      print "\t.zero %u" Config.word;
      env'
  (* local variable: add it to the context and anticipate its size in the stack *)
  | CDECL (loc,name) ->
      let obj = {name; otype = Var; storage = Stack (env.off-1)} in
      let env' = {env with off = env.off-1; objs = obj::env.objs} in
      env'
  (* global function: add it to the context and compile it *)
  | CFUN (loc,name,params,loccode) when toplevel ->
      let name = function_name name in
      if object_exists env.objs name then
        error (Some loc) "redefinition of global object ‘%s’" name;
      let nparams = List.length params in
      let obj = {name; otype = Func nparams; storage = Static} in
      let env' = {env with objs = obj::env.objs} in
      print "";
      print ".section .text";
      print "\t.globl\t%s" name;
      print "\t.type\t%s, @function" name;
      print "  %s:" name;
      (* Separate parameters which have been passed through registers and those
         pushed on the stack. *)
      let (first_params, registers) =
       List.split (Util.list_zip_truncate params Config.call_registers) in
      let n = List.length first_params in
      let last_params = Util.list_drop n params in
      let p = nparams-n in
      (* Add those in the stack to the local environment; their offsets are
         1, 2, …, p since there is the return address at offset 0. *)
      let env2 = compile_param_list {env' with off = p+1} last_params in
      (* Push those in registers to the stack and add them to the local
         environment too. *)
      let env2 = compile_param_list {env2 with off = 0} first_params in
      List.iter (print "\tpushq\t%s") (List.rev registers);
      (* Compile the function’s body. *)
      compile_loccode env2 loccode;
      (* Return (in case the function’s body does not return itself); this
         will also free the space allocated in the stack. *)
      return env2;
      env'
  (* local function: not permitted by C-- *)
  | CFUN (loc,_,_,_) ->
      error (Some loc) "local functions are not permitted";
      env

and compile_loccode env (loc,code) =
  compile_code env loc code

(* Compile commands and whole blocks.
   Return the resulting environment. *)
and compile_code env loc = function
  (* block of code, including declaration of local variables: *)
  | CBLOCK (declarations,loccodes) ->
      (* Add the local variables to the environment and allocate space for them
         in the stack. *)
      let env' = compile_decl_list false env declarations in
      allocate (env.off - env'.off);
      (* Compile the contents of the block. *)
      List.iter (compile_loccode env') loccodes;
      (* Free the allocated space, if any. *)
      free (env.off - env'.off)
  (* single command made of an expression: *)
  | CEXPR locexpr ->
      compile_locexpr env locexpr |> ignore
  (* “if-then-else” construct: *)
  | CIF (condition,loccode1,loccode2) ->
      let label_else = Genlab.genlab "if_else" in
      let label_end  = Genlab.genlab "if_end" in
      (* if… *)
      let res = compile_locexpr env condition |> store_if_cst Config.reg0 in
      print "\tcmpq\t$0, %s" res;
      print "\tje\t%s" label_else;
      (* … then… *)
      compile_loccode env loccode1;
      print "\tjmp\t%s" label_end;
      (* … else… *)
      print "  %s:" label_else;
      compile_loccode env loccode2;
      print "  %s:" label_end
  (* “while” loop *)
  | CWHILE (condition,loccode) ->
      let label_begin = Genlab.genlab "loop_begin" in
      let label_end = Genlab.genlab "loop_end" in
      (* while… *)
      print "  %s:" label_begin;
      let res = compile_locexpr env condition |> store_if_cst Config.reg0 in
      print "\tcmpq\t$0, %s" res;
      print "\tje\t%s" label_end;
      (* … do… *)
      compile_loccode env loccode;
      print "\tjmp\t%s" label_begin;
      print "  %s:" label_end
  (* return instruction: *)
  | CRETURN opt_locexpr ->
      begin match opt_locexpr with
      | None         -> ()
      | Some locexpr -> compile_locexpr env locexpr |> store0
      end;
      return env
  (* exception throwing: *)
  | CTHROW (excname,locexpr) ->
      let exc = exception_code excname in
      compile_locexpr env locexpr |> store0;
      print "\tmovq\t$%u, %s" exc Config.reg1;
      throw env
  (* “try…with” construct: *)
  | CTRY (loccode,handlers,opt_finally_loccode) ->
      let has_finally = opt_finally_loccode <> None in
      let label_return   = Genlab.genlab "try_return"
      and label_handlers = Genlab.genlab "try_handlers"
      and label_finally  = Genlab.genlab "try_finally"
      and label_rereturn = Genlab.genlab "try_rereturn"
      and label_end      = Genlab.genlab "try_end" in
      (* “Try” block header. *)
      print "\tpushq\t%%rbp";
      print "\tmovq\t%%rsp, %%rbp";
      print "\tpushq\t$%s" label_handlers;
      (* Normal code. *)
      let env' = {env with off = env.off-2; finally = Some label_return} in
      compile_loccode env' loccode;
      free 1;
      print "\tpopq\t%%rbp";
      if has_finally then begin
        print "\tpushq\t$0";
        print "\tpushq\t$%u" exccode_nothing
      end;
      print "\tjmp\t%s" label_finally;
      (* Return point. *)
      print "  %s:" label_return;
      print "\tpopq\t%%rbp";
      if has_finally then begin
        print "\tpushq\t%s" Config.reg0;
        print "\tpushq\t$%u" exccode_return
      end;
      print "\tjmp\t%s" label_finally;
      (* Exception handlers. *)
      print "  %s:" label_handlers;
      print "\tpopq\t%%rbp";
      if has_finally then begin
        print "\tpushq\t%s" Config.reg0;
        print "\tpushq\t$%u" exccode_nothing
      end;
      List.iter
       (fun (excname,name,handler_loccode) ->
         let label_handler_end = Genlab.genlab ("try_handler_end_" ^ excname) in
         let exc = exception_code excname in
         print "\tcmpq\t$%u, %s" exc Config.reg1;
         print "\tjne\t%s" label_handler_end;
         if not has_finally then
           print "\tpushq\t%s" Config.reg0;
         let env1 = compile_decl false env (CDECL(loc,name)) in
         let env2 = if has_finally then {env1 with off = env1.off-1} else env1 in
         compile_loccode env2 handler_loccode;
         if not has_finally then
           free 1;
         print "\tjmp\t%s" label_finally;
         print "  %s:" label_handler_end;
       )
       handlers;
      begin match opt_finally_loccode with
      | Some finally_loccode ->
          (* For uncaught exceptions: memorize the exception to be propagated. *)
          print "\tmovq\t%s, (%%rsp)" Config.reg1;
          (* “Finally” clause. *)
          print "  %s:" label_finally;
          let env'' = {env with off = env.off-2} in
          compile_loccode env'' finally_loccode;
          (* Propagation of exceptions/returns. *)
          print "\tpopq\t%s" Config.reg1;
          print "\tpopq\t%s" Config.reg0;
          print "\tcmpq\t$%u, %s" exccode_nothing Config.reg1;
          print "\tje\t%s" label_end;
          print "\tcmpq\t$%u, %s" exccode_return Config.reg1;
          print "\tje\t%s" label_rereturn;
          throw env;
          print "  %s:" label_rereturn;
          return env;
          print "  %s:" label_end;
      | _ ->
          (* Propagate uncaught exceptions. *)
          throw env;
          print "  %s:" label_finally
      end

(* Functions used to move a value to a given register if that value meets some
   condition. The aim is e.g. to force the result of ‘compile_locexpr’ to be in
   %rax (see ‘store0’) or another register. *)
and store_if cond reg res =
  if cond res then begin
    print "\tmovq\t%s, %s" res reg;
    reg
  end else
    res
and store_if_mem reg     = store_if (fun res -> String.contains res '(') reg
and store_if_cst reg     = store_if (fun res -> res.[0] = '$')           reg
and store        reg res = store_if ((<>) reg)                           reg res |> ignore
and store0           res = store Config.reg0 res

and compile_locexpr env (loc,expr) =
  compile_expr env loc expr

(* Compile an expression.
   Return a bit of asssembly that designates the value computed (‘Config.reg0’
   most of the time. *)
and compile_expr env loc = function
  (* string literal: *)
  | STRING value ->
      let label = Genlab.genlab "string_literal" in
      (* We need to store the string somewhere in a data section. *)
      pending "";
      pending ".section .data";
      pending "\t.local\t%s" label;
      pending "\t.type\t%s, @object" label;
      pending "\t.align\t4";
      pending "\t.size\t%s, %u" label (String.length value + 1);
      pending "  %s:" label;
      pending "\t.string \"%s\"" (String.escaped value);
      "$" ^ label
  (* integer constant: *)
  | CST value ->
      "$" ^ string_of_int value
  (* variable name: *)
  | VAR name ->
      get_variable env loc name
  (* assignation to a variable: *)
  | SET_VAR (name,locexpr) ->
      let res = compile_locexpr env locexpr |> store_if_mem Config.reg0 in
      print "\tmovq\t%s, %s" res (get_variable env loc name);
      res
  (* assignation to a subscripted variable: *)
  | SET_ARRAY (name,loci,locexpr) ->
      (* A generic solution: *)
      (*let res = compile_locexpr env locexpr in
        print "\tpushq\t%s" res;
        let env' = {env with off = env.off-1} in
        let lvalue = compile_lvalue env' loc (OP2(S_INDEX,(loc,VAR name),loci)) in
        print "\tpopq\t%s" lvalue;
        print "\tmovq\t%i(%%rsp), %s" (-Config.word) Config.reg0;
        Config.reg0*)
      (* We can optimize this a bit by knowing that the subscripted expression
         is a variable name: *)
      let res = compile_locexpr env locexpr in
      print "\tpushq\t%s" res;
      let env' = {env with off = env.off-1} in
      compile_locexpr env' loci |> store0;
      print "\tmovq\t%s, %s" (get_variable env' loc name) Config.reg1;
      print "\tpopq\t(%s,%s,%u)" Config.reg1 Config.reg0 Config.word;
      print "\tmovq\t%i(%%rsp), %s" (-Config.word) Config.reg0;
      Config.reg0
  (* function call: *)
  | CALL (name,locexprs) ->
      let nargs = List.length locexprs in
      let name = get_function env loc name nargs in
      let (_, first_regs) =
       List.split (Util.list_zip_truncate locexprs Config.call_registers) in
      let p = nargs - (List.length first_regs) in
      (* We must ensure that the stack pointer is 16-byte-aligned before
         calling a function. *)
      let padding = (p - env.off) mod 2 in
      allocate padding;
      let off' = env.off - padding in
      (* Compute each argument from right to left and push them to the stack. *)
      List.iteri
       (fun i arg ->
         let res = compile_locexpr {env with off = off'-i} arg in
         print "\tpushq\t%s" res
       )
       (List.rev locexprs);
      (* Put the first six to registers *)
      List.iter (fun reg -> print "\tpopq\t%s" reg) first_regs;
      (* Call the function (some standard functions need %rax to be 0). *)
      print "\tmovq\t$0, %%rax";
      print "\tcallq\t%s" name;
      (** patch begin /************************************)
      (* Some standard functions incorrectly behave in a 32-bit fashion; this
         hack fix some issues. *)
      if not (object_exists env.objs name) && (name <> "malloc") then begin
        if Config.debug then
          print "\t# hack for stdlib functions with a 32-bit behaviour:";
        print "\tmovslq\t%%eax, %%rax"
      end;
      (**************************************/ patch end **)
      (* Restore the stack. *)
      free (padding + p);
      Config.reg0
  (* prefix or postfix ++/-- (that is, unary operator modifying a lvalue): *)
  | OP1 (monop,locexpr) when monop <> M_MINUS && monop <> M_NOT ->
      let lvalue = compile_loclvalue env locexpr in
      begin match monop with
      | M_POST_INC ->
          print "\tmovq\t%s, %s" lvalue Config.reg0;
          print "\tincq\t%s"     lvalue
      | M_POST_DEC ->
          print "\tmovq\t%s, %s" lvalue Config.reg0;
          print "\tdecq\t%s"     lvalue
      | M_PRE_INC  ->
          print "\tincq\t%s"     lvalue;
          print "\tmovq\t%s, %s" lvalue Config.reg0
      | M_PRE_DEC  ->
          print "\tdecq\t%s"     lvalue;
          print "\tmovq\t%s, %s" lvalue Config.reg0
      | _ -> raise Wont_happen
      end;
      Config.reg0
  (* other unary operator: *)
  | OP1 (monop,locexpr) ->
      compile_locexpr env locexpr |> store0;
      begin match monop with
      | M_MINUS -> print "\tnegq\t%s" Config.reg0
      | M_NOT   -> print "\tnotq\t%s" Config.reg0
      | _ -> raise Wont_happen
      end;
      Config.reg0
  (* binary operator (except comparison operators): *)
  | OP2 (binop,locexpr1,locexpr2) ->
      let res2 = compile_both_locexprs env locexpr1 locexpr2 in
      begin match binop with
      | S_ADD   -> print "\taddq\t%s, %s"  res2 Config.reg0
      | S_SUB   -> print "\tsubq\t%s, %s"  res2 Config.reg0
      | S_MUL   -> print "\timulq\t%s, %s" res2 Config.reg0
      | S_DIV   ->
          let res2 = store_if_cst Config.reg1 res2 in
          print "\tcqto";
          print "\tidivq\t%s" res2
      | S_MOD   ->
          let res2 = store_if_cst Config.reg1 res2 in
          print "\tcqto";
          print "\tidivq\t%s" res2;
          print "\tmovq\t%%rdx, %s" Config.reg0
      | S_INDEX ->
          store Config.reg1 res2;
          print "\tmovq\t(%s,%s,%u), %s" Config.reg0 Config.reg1 Config.word Config.reg0
      end;
      Config.reg0
  (* comparison operator: *)
  | CMP (cmpop,locexpr1,locexpr2) ->
      let label_end  = Genlab.genlab "cmp_end" in
      let cmp = begin match cmpop with
       | C_LT -> "l"
       | C_LE -> "le"
       | C_EQ -> "e"
      end in
      let res2 = compile_both_locexprs env locexpr1 locexpr2 in
      print "\tcmpq\t%s, %s" res2 Config.reg0;
      print "\tmovq\t$1, %s" Config.reg0;
      print "\tj%s\t%s" cmp label_end;
      print "\tmovb\t$0, %%al";
      print "  %s:" label_end;
      Config.reg0
  (* ternary operator (‘a?b:c’): *)
  | EIF (locexpr1,locexpr2,locexpr3) ->
      let label_else = Genlab.genlab "ternary_else" in
      let label_end  = Genlab.genlab "ternary_end" in
      let res1 = compile_locexpr env locexpr1 |> store_if_cst Config.reg0 in
      print "\tcmpq\t$0, %s" res1;
      print "\tje\t%s" label_else;
      compile_locexpr env locexpr2 |> store0;
      print "\tjmp\t%s" label_end;
      print "  %s:" label_else;
      compile_locexpr env locexpr3 |> store0;
      print "  %s:" label_end;
      Config.reg0
  (* sequence operator (‘a,b’): *)
  | ESEQ locexprs ->
      List.fold_left (fun _ -> compile_locexpr env) Config.reg0 locexprs

and compile_loclvalue env (loc,expr) =
  compile_lvalue env loc expr

(* Compile a lvalue, that is, return a bit of assembly code with which the
   lvalue can be refered to, possibly after having produced some assembly
   instructions for preliminary computation (must respect the same conventions
   as the evaluation of expressions, and the returned value might depend on
   the contents of ‘Config.reg1’). *)
and compile_lvalue env loc = function
  | VAR name ->
      get_variable env loc name
  | OP2 (S_INDEX,locexpr1,locexpr2) ->
      let res2 = compile_both_locexprs env locexpr1 locexpr2 in
      store Config.reg1 res2;
      print "\tleaq\t(%s,%s,%u), %s" Config.reg0 Config.reg1 Config.word Config.reg1;
      "(" ^ Config.reg1 ^ ")"
  | _ ->
      error (Some loc) "this is not a lvalue";
      ""

(* Compile both expressions given; stores the result of the first (evaluated
   second) in ‘Config.reg0’ and return that of the second as ‘compile_locexpr’
   does (‘Config.reg1’ most of the time). *)
and compile_both_locexprs env locexpr1 locexpr2 =
  let res2 = compile_locexpr env locexpr2 in
  if res2.[0] <> '$' then begin
    print "\tpushq\t%s" res2;
    let env' = {env with off = env.off-1} in
    compile_locexpr env' locexpr1 |> store0;
    print "\tpopq\t%s" Config.reg1;
    Config.reg1
  end else begin
    compile_locexpr env locexpr1 |> store0;
    res2
  end

(* Produce the bit of assembly for returning. *)
and return env =
  match env.finally with
  | None       -> free (-env.off);
                  print "\tretq"
  | Some label -> print "\tmovq\t%%rbp, %%rsp";
                  print "\tjmp\t%s" label

(* Produce the bit of assembly for throming an error. *)
and throw env =
  print "\tmovq\t%%rbp, %%rsp";
  print "\tjmp\t*%i(%%rsp)" (-Config.word)

(* Allocate the given number of words in the stack. *)
and allocate words =
  if words <> 0 then
    print "\tsubq\t$%u, %%rsp" (Config.word * words)

(* Free the given number of words in the stack. *)
and free words =
  if words <> 0 then
    print "\taddq\t$%u, %%rsp" (Config.word * words)

(* Return a bit of assembly code designating the variable whose name is given. *)
and get_variable env loc name =
  try
    let obj = find_object env.objs name in
    if obj.otype <> Var then
      error (Some loc) "object ‘%s’ is not a variable" name;
    begin match obj.storage with
      | Static        -> Printf.sprintf "%s" name
      | Stack obj_off -> Printf.sprintf "%i(%%rsp)" (Config.word * (obj_off - env.off))
      | Register reg  -> Printf.sprintf "%s" reg
    end
  with Not_found ->
    if Config.debug then
      warning (Some loc) "unknown variable ‘%s’ (declared elsewhere?)" name;
    name

(* Same thing for functions. Actually, as only static functions are supported,
   just do some checkings (which are not strictly necessary for compiling). *)
and get_function env loc name nargs =
  let name = function_name name in
  try
    let obj = find_object env.objs name in
    begin match obj.otype with
      | Var ->
          error (Some loc) "object ‘%s’ is not a function" name
      | Func nparams -> if nparams <> nargs then
          warning (Some loc) "calling ‘%s’ with %u argument(s) but expecting %u" name nargs nparams
    end;
    if obj.storage <> Static then
      raise (Not_implemented "non-global functions");
    name
  with Not_found ->
    if Config.debug then
      warning (Some loc) "unknown function ‘%s’ (declared elsewhere?)" name;
    name
