open Error
open Cparse
open Format

(** TD9 Exercice 1 (inutile pour le vrai projet) **)

let print_listFun out dec_list =
  let rec merge_strings sep = function
    | []    -> ""
    | s::[] -> s
    | s::q  -> s ^ sep ^ (merge_strings sep q)
  in
  let rec to_str = function
    | CDECL (_, name)            -> Printf.sprintf "int %s"     name
    | CFUN  (_, name, params, _) -> Printf.sprintf "int %s(%s)" name
                                                         (list_to_str params)
  and list_to_str dec_list =
    dec_list
    |> List.map to_str
    |> merge_strings ", "
  in
  List.iter (fun dec -> Printf.fprintf out "%s;\n" (to_str dec)) dec_list

(** TD9 Exercice 2 (inutile pour le vrai projet) **)

type listImbr = F of string | I of listImbr list

let print_listImbr out li indent =
  (* Ne marche pas pour une raison obscure : *)
  (*Format.fprintf (Format.formatter_of_out_channel stdout) "Babar";*)
  let rec print_rec = function
    | F s -> Format.printf "@\n%s" s
    | I l -> Format.printf "@\n@[<hov %d>{" indent;
             List.iter print_rec l;
             Format.printf "@]@\n}"
  in print_rec li

(** TD9 Exercice 3 **)

let print_locator out nom fl fc ll lc = 
  Printf.fprintf out "file=\"%s\" first-line=\"%u\" first-column=\"%u\" last-line=\"%u\" last-column=\"%u\"" nom fl fc ll lc

type xml =
 | XmlText of string
 | XmlNode of string * xml_attribute list * xml list
and xml_attribute = string*string

let print_xml out indent =
  let rec print_rec = function
  | XmlText t ->
      Format.fprintf out "@\n%s" t
  | XmlNode (tag,attributes,children) ->
      Format.fprintf out "@\n@[<hov %d><%s" indent tag;
      List.iter (fun (name,value) -> Format.fprintf out " %s=\"%s\"" name value) attributes;
      if children = [] then
        Format.fprintf out "/>@]"
      else begin
        Format.fprintf out ">";
        List.iter print_rec children;
	Format.fprintf out "@]@\n</%s>" tag
      end
  in
  print_rec

let rec xml_of_arbreSyntaxe dec_list =
  XmlNode ("program", [], (List.map xml_of_declaration dec_list))

and xml_of_locator (nom,fl,fc,ll,lc) =
  let s = string_of_int in
  ["file", nom; "first-line", s fl; "first-column", s fc; "last-line", s ll; "last-column", s lc]

and xml_of_declaration = function
  | CDECL (loc,name) ->
      XmlNode ("cdecl", ("name", name) :: xml_of_locator loc, [])
  | CFUN  (loc,name,params,loccode) ->
      XmlNode ("cfunc", ("name", name) :: xml_of_locator loc, [
          XmlNode ("args", [], List.map xml_of_declaration params);
          xml_of_loccode loccode;
      ])

and xml_of_loccode (loc,code) =
  match xml_of_code code with
  | XmlNode (name,attributes,children) ->
         XmlNode (name, attributes @ xml_of_locator loc, children)
  | _ -> failwith "Won’t happen."

and xml_of_code = function
  | CBLOCK (declarations,loccodes) ->
      XmlNode ("cblock", [],
          List.map xml_of_declaration declarations @ List.map xml_of_loccode loccodes
      )
  | CEXPR locexpr ->
      XmlNode ("cexpr", [], [xml_of_locexpr locexpr])
  | CIF (condition,loccode1,loccode2) ->
      XmlNode ("cif", [], [xml_of_locexpr condition; xml_of_loccode loccode1; xml_of_loccode loccode2])
  | CWHILE (condition,loccode) ->
      XmlNode ("cwhile", [], [xml_of_locexpr condition; xml_of_loccode loccode])
  | CRETURN opt_locexpr ->
      let contents = 
       begin match opt_locexpr with
       | None         -> []
       | Some locexpr -> [xml_of_locexpr locexpr]
       end in
      XmlNode ("creturn", [], contents)
  | CTHROW (excname,locexpr) ->
      XmlNode ("cthrow", ["name",excname], [xml_of_locexpr locexpr])
  | CTRY (loccode,handlers,finally) ->
      let xml_of_handler (excname,varname,loccode) =
        XmlNode ("catch", ["name",excname; "varname",varname], [xml_of_loccode loccode])
      in
      let finally_node =
       begin match finally with
       | None         -> []
       | Some loccode -> [XmlNode ("finally", [], [xml_of_loccode loccode])]
       end in
      XmlNode ("ctry", [], xml_of_loccode loccode :: (List.map xml_of_handler handlers @ finally_node))

and xml_of_locexpr (loc,expr) =
  match xml_of_expr expr with
  | XmlNode (name,attributes,children) ->
         XmlNode (name, attributes @ xml_of_locator loc, children)
  | _ -> failwith "Won’t happen."

and xml_of_expr = function
  | VAR name                      -> XmlNode ("var", ["name", name], [])
  | CST value                     -> XmlNode ("cst", ["value", string_of_int value], [])
  | STRING value                  -> XmlNode ("string", ["value", value], [])
  | SET_VAR (name,locexpr)        -> XmlNode ("set_var", ["name", name], [xml_of_locexpr locexpr])
  | SET_ARRAY (name,loci,locexpr) -> XmlNode ("set_array", ["name", name], [xml_of_locexpr loci; xml_of_locexpr locexpr])
  | CALL (name,locexprs)          -> XmlNode ("call", ["name", name], List.map xml_of_locexpr locexprs)
  | OP1 (monop,locexpr)           -> XmlNode ("op1", ["op", string_of_monop monop], [xml_of_locexpr locexpr])
  | OP2 (binop,locexpr1,locexpr2) -> XmlNode ("op2", ["op", string_of_binop binop], [xml_of_locexpr locexpr1; xml_of_locexpr locexpr2])
  | CMP (cmpop,locexpr1,locexpr2) -> XmlNode ("cmp", ["op", string_of_cmpop cmpop], [xml_of_locexpr locexpr1; xml_of_locexpr locexpr2])
  | EIF (locexpr1,locexpr2,locexpr3) -> XmlNode ("eif", [], [xml_of_locexpr locexpr1; xml_of_locexpr locexpr2; xml_of_locexpr locexpr3])
  | ESEQ locexprs                 -> XmlNode ("eseq", [], List.map xml_of_locexpr locexprs)

and string_of_monop = function
  | M_MINUS    -> "minus"
  | M_NOT      -> "not"
  | M_POST_INC -> "post_inc"
  | M_POST_DEC -> "post_dec"
  | M_PRE_INC  -> "pre_inc"
  | M_PRE_DEC  -> "pre_dec"

and string_of_binop = function
  | S_MUL   -> "mul"
  | S_DIV   -> "div"
  | S_MOD   -> "mod"
  | S_ADD   -> "add"
  | S_SUB   -> "sub"
  | S_INDEX -> "index"

and string_of_cmpop = function
  | C_LT -> "lt"
  | C_LE -> "le"
  | C_EQ -> "eq"

let print_arbreSyntaxe out dec_list =
  (*print_listFun out dec_list;*)
  (*print_listImbr out
   (I [
        F "Babar";
        F "Zébulon";
        I [
            F "Tournesol";
            I [
                I [
                    F "Soja";
                ];
            ];
            I [];
            F "Formidable"
        ]
    ]
   ) 8;*)
  dec_list
  |> xml_of_arbreSyntaxe
  |> print_xml std_formatter 4
