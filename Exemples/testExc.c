int f(int a, int b, int c)
{
	if (a)
		throw ExcA a;
	if (b)
		throw ExcB b;
	try {
		if (!c)
			throw ExcCZero (-1);
	}
	catch (ExcUseless _) {}
	finally {
		if (c)
			return -c;
	}
	return 0;
}

int test(int i)
{
	printf("test %i\n", i);
}

int n;

int macro(int u, int v, int w)
{
	int x;
	
	x = 1337;
	test(++n);
	
	try {
		x = f(u, v, w);
	}
	catch (ExcA a) {
		printf("%i: ExcA %i, x = %i\n", n, a, x);
	}
	catch (ExcB b) {
		printf("%i: ExcB %i, x = %i\n", n, b, x);
	}
	/*catch (ExcCZero c) {
		printf("%i: ExcCZero %i, x = %i\n", n, c, x);
	}*/
	finally {
		printf("%i: finally, x = %i\n", n, x);
	}
	
	printf("%i: x = %i\n", n, x);
	
	return n;
}

int main(int ac, int av)
{
	n = 0;
	macro(13,37,0xF00L); // note: 0xF00 == 3840
	macro(0,37,0xF00L);
	macro(0,0,0xF00L);
	macro(0,0,0);
	
	printf("never printed because of an uncaught exception ExcCZero\n");
	
	return 42;
}
