int main(int ac, int av)
{
	printf("1:%i\n",  0 <   1);

	printf("1:%i\n", -1 <   0);
	printf("1:%i\n", -1 <=  0);
	printf("0:%i\n", -1 >   0);
	printf("0:%i\n", -1 >=  0);

	printf("1:%i\n", -2 <  -1);
	printf("1:%i\n", -2 <= -1);
	printf("0:%i\n", -2 >  -1);
	printf("0:%i\n", -2 >= -1);

	printf("0:%i\n", -1 <  -1);
	printf("1:%i\n", -1 <= -1);
	printf("0:%i\n", -1 >  -1);
	printf("1:%i\n", -1 >= -1);
}
