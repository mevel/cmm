.noexc.noopt:
	sans les exceptions
	avant optimisation (fichier rendu pour la première partie du projet)

.noexc.opt1:
	sans les exceptions
	après la première phase d’optimisation (utilisation des registres)

.noexc.opt2:
	sans les exceptions
	après la seconde phase d’optimisation (mettre directement le résultat d’un calcul
	dans la bonne destination pour économiser des instructions) (inachevé)

.exc.ret_as_exc.noopt:
	avec les exceptions (« return » est une exception)
	sans optimisation

.exc.noopt
	avec les exceptions
	message d’erreur sur stderr pour les exceptions non rattrappées
	factorisation (fonction ‘compile_both_locexprs’, etc.)
	optimisations locales
	optimisation du « try » sans « finally »
	sans optimisation globale de l’utilisation des registres

.exc.opt
	comme ci-dessus
	plus de généricité dans les registres utilisés (‘Config.reg0’, etc.)
	optimisation : suppression d’intermédiaires, on met directement les résultats
	dans la destination souhaitée le plus souvent possible
