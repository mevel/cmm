open Cparse
(*open Genlab*)


(** Output utilities (printf-like). **)

(* Writing assembly code on the output file. *)
let global_out = ref stdout
let print fmt = Printf.kfprintf (fun f -> output_char f '\n') !global_out fmt

(* Delayed writing of assembly code. *)
let pending_buf = Buffer.create 1024
let pending fmt = Printf.kbprintf (fun b -> Buffer.add_char b '\n') pending_buf fmt
let print_pending () = Buffer.output_buffer !global_out pending_buf

(* Errors and warnings. *)
let warning loc fmt = Printf.ksprintf (Error.warning loc) fmt
let error   loc fmt = Printf.ksprintf (Error.error   loc) fmt


(** Useful stuffs. **)

module Util = struct
  let rec list_drop n l =
    if n = 0 then l else list_drop (n-1) (List.tl l)
  let rec list_zip_truncate l m = match l, m with
    | [], _
    | _, []        -> []
    | a::l', b::m' -> (a,b) :: list_zip_truncate l' m'
end


(** Don’t worry, this won’t occur. **)

exception Not_implemented of string
exception Wont_happen

let fooloc = ("",0,0,0,0)

let exceptions = Hashtbl.create 256
let exceptions_count = ref 1
let exception_code name =
  try
    Hashtbl.find exceptions name
  with Not_found ->
    let x = !exceptions_count in
    Hashtbl.add exceptions name x;
    incr exceptions_count;
    x


(** Configuration of the compiler (mainly intended to ease switching between
 ** 32-bit and 64-bit architecture) **)

  (* TODO: find an elegant way to parametrize the mnemonic’s suffix (l or q),
     register names and all that stuff… *)

module Config = struct
  (* print some debug messages: *)
  let debug = true
  (* size, in bytes, of a machine word: *)
  let word = 8
  (* registers used by the calling convention for integer arguments, in order: *)
  let call_registers = ["rdi"; "rsi"; "rdx"; "rcx"; "r8"; "r9"]
end


(** Representation of an environment, that is, the set of known objects
 ** in the current scope and their location in memory). **)

(* where and how an object is stored: *)
type obj_storage =
  | Static
  | Stack of int          (* stack offset (in words) *)
  | Register of string    (* register name *)
(* the type of an object (C-- only comprises int variables and functions): *)
type obj_type =
  | Var
  | Func of int           (* number of parameters *)
(* an object: *)
type obj = {name: string; otype: obj_type; storage: obj_storage}
(* an environment: *)
type env = int * obj list  (* current stack offset (in words), known objects *)

(* Newly declared objects are added to the head of the environment. When
 * looking for an object by its name, we select the first object with that
 * name appearing in the list, that is the last declared one. A local variable
 * temporarily hides the objects of the same name declared in a containing scope.
 *)

let object_exists objs name =
  List.exists (fun o -> o.name = name) objs
let find_object objs name =
  List.find (fun o -> o.name = name) objs


(** The compilation functions. **)

(* Conventions:
 *   Function calls follow the GCC convention for 64-bit architecture:
 *   — first six (integer) arguments in %rdi, %rsi, %rdx, %rcx, %r8, %r9,
 *     the remaining in the stack after the return address (7th argument
 *     being the leftmost);
 *   — each function must ensure that %rsp, %rbp, %r12, %r15 will not be
 *     altered after their return;
 *   — the result value is found in %rax.
 *   Additionally, for the sake of simplicity, the evaluation of an expression
 *   (and sub-expression) also satisfies the last two constraints.
 *)

(* The main compilation function. *)
let rec compile out decl_list =
  global_out := out;
  compile_decl_list true (0,[]) decl_list |> ignore;
  print_pending ()

(* Compile a list of declarations, whether global or local.
   Return the resulting environment. *)
and compile_decl_list toplevel (*env decl_list*) =
  List.fold_left (compile_decl toplevel) (*env decl_list*)

(* Add the given list of function parameters to the environment, without
   actually producing the code to allocate space on the stack (only computes
   the stack offset of each variable and the resulting new position of the
   stack pointer).
   Return the resulting environment. *)
and compile_param_list env param_list =
  compile_decl_list false env (List.rev param_list)

(* Compile a single declaration, global or local.
   Return the resulting environment. *)
and compile_decl toplevel (off,objs as env) = function
  (* global variable: add it to the context and allocate it in a data section *)
  | CDECL (loc,name) when toplevel ->
      if object_exists objs name then
        error (Some loc) "redefinition of global object ‘%s’" name;
      let obj = {name; otype = Var; storage = Static} in
      let env' = (off, obj::objs) in
      (* Note: C-- does not support initialization of variables, thus we can
         put them in the BSS section. *)
      print "";
      print ".section .bss";
      print "\t.globl\t%s" name;
      print "\t.type\t%s, @object" name;
      print "\t.align\t4";
      print "\t.size\t%s, %u" name Config.word;
      print "  %s:" name;
      print "\t.zero %u" Config.word;
      env'
  (* local variable: add it to the context and anticipate its size in the stack *)
  | CDECL (loc,name) ->
      let obj = {name; otype = Var; storage = Stack (off-1)} in
      let env' = (off-1, obj::objs) in
      env'
  (* global function: add it to the context and compile it *)
  | CFUN (loc,name,params,loccode) when toplevel ->
      if object_exists objs name then
        error (Some loc) "redefinition of global object ‘%s’" name;
      let nparams = List.length params in
      let obj = {name; otype = Func nparams; storage = Static} in
      let (_,objs') as env' = (off, obj::objs) in
      print "";
      print ".section .text";
      print "\t.globl\t%s" name;
      print "\t.type\t%s, @function" name;
      print "  %s:" name;
      (* Separate parameters which have been passed through registers and those
         pushed on the stack. *)
      let (first_params, registers) =
       List.split (Util.list_zip_truncate params Config.call_registers) in
      let n = List.length first_params in
      let last_params = Util.list_drop n params in
      let p = nparams-n in
      (* Add those in the stack to the local environment; their offsets are
         1, 2, …, p since there is the return address at offset 0. *)
      let (_,objs2) = compile_param_list (p+1,objs') last_params in
      (* Push those in registers to the stack and add them to the local
         environment too. *)
      let env2 = compile_param_list (0,objs2) first_params in
      List.iter (print "\tpushq\t%%%s") (List.rev registers);
      (* Compile the function’s body. *)
      (*compile_loccode env2 loccode;
      (* Return (in case the function’s body does not return itself); this
         will also free the space allocated in the stack. *)
      compile_code env2 (CRETURN None);*)
      compile_loccode env2 (fooloc, CTRY (loccode, ["","",(fooloc,CBLOCK([],[]))], None));
      (* Free the space allocated in the stack. *)
      let (off2,_) = env2 in
      free (-off2);
      (* Return. *)
      print "\tretq";
      env'
  (* local function: not permitted by C-- *)
  | CFUN (loc,_,_,_) ->
      error (Some loc) "local functions are not permitted";
      env

and compile_loccode env (loc,code) =
  compile_code env code

(* Compile commands and whole blocks.
   Return the resulting environment. *)
and compile_code (off,objs as env) = function
  (* block of code, including declaration of local variables: *)
  | CBLOCK (declarations,loccodes) ->
      (* Add the local variables to the environment and allocate space for them
         in the stack. *)
      let (off',_) as env' = compile_decl_list false env declarations in
      allocate (off-off');
      (* Compile the contents of the block. *)
      List.iter (compile_loccode env') loccodes;
      (* Free the allocated space, if any. *)
      free (off-off')
  (* single command made of an expression: *)
  | CEXPR locexpr ->
      compile_locexpr env locexpr
  (* “if-then-else” construct: *)
  | CIF (condition,loccode1,loccode2) ->
      let label_else = Genlab.genlab "if_else" in
      let label_end  = Genlab.genlab "if_end" in
      (* if… *)
      compile_locexpr env condition;
      print "\tcmpq\t$0, %%rax";
      print "\tje\t%s" label_else;
      (* … then… *)
      compile_loccode env loccode1;
      print "\tjmp\t%s" label_end;
      (* … else… *)
      print "  %s:" label_else;
      compile_loccode env loccode2;
      print "  %s:" label_end
  (* “while” loop *)
  | CWHILE (condition,loccode) ->
      let label_begin = Genlab.genlab "loop_begin" in
      let label_end = Genlab.genlab "loop_end" in
      (* while… *)
      print "  %s:" label_begin;
      compile_locexpr env condition;
      print "\tcmpq\t$0, %%rax";
      print "\tje\t%s" label_end;
      (* … do… *)
      compile_loccode env loccode;
      print "\tjmp\t%s" label_begin;
      print "  %s:" label_end
  (* return instruction: *)
  (*| CRETURN opt_locexpr ->
      (* Compute the return value and store it in %rax. *)
      begin match opt_locexpr with
        | Some locexpr -> compile_locexpr env locexpr
        | _ -> ()
      end;
      (*(* Free the space allocated in the stack. *)
      free (-off);
      (* Return. *)
      print "\tretq"*)
      let exc = exception_code "" in
      print "\tmovq\t$%i, %%r10" exc;
      print "\tmovq\t%%rbp, %%rsp";
      print "\tjmp\t*(%%rsp)"
  *)
  | CRETURN None ->
      compile_code env (CTHROW("",(fooloc,CST 0xF00)))
  | CRETURN (Some locexpr) ->
      compile_code env (CTHROW("",locexpr))
  | CTHROW (excname,locexpr) ->
      let exc = exception_code excname in
      compile_locexpr env locexpr;
      print "\tmovq\t$%i, %%r10" exc;
      print "\tmovq\t%%rbp, %%rsp";
      print "\tjmp\t*(%%rsp)"
  | CTRY (loccode,handlers,None) ->
      compile_code env (CTRY (loccode, handlers, Some (fooloc,CBLOCK([],[]))))
  | CTRY (loccode,handlers,Some finally_loccode) ->
      let label_finally  = Genlab.genlab "try_finally"
      and label_handlers = Genlab.genlab "try_handlers"
      and label_end      = Genlab.genlab "try_end" in
      (* “Try” block header. *)
      print "\tpushq\t%%rbp";
      print "\tpushq\t$%s" label_handlers;
      print "\tmovq\t%%rsp, %%rbp";
      let env' = (off-2,objs) in
      (* Normal code. *)
      compile_loccode env' loccode;
      print "\tpopq\t%%rbp"; (* foo *)
      print "\tpopq\t%%rbp";
      print "\tpushq\t$0";
      print "\tjmp\t%s" label_finally;
      (* Exception handlers. *)
      print "  %s:" label_handlers;
      print "\tpopq\t%%rbp"; (* foo *)
      print "\tpopq\t%%rbp";
      print "\tpushq\t$0";
      let env' = (off-1,objs) in
      List.iter
       (fun (excname,name,handler_loccode) ->
         let label_handler_end = Genlab.genlab ("try_handler_end_" ^ excname) in
         let exc = exception_code excname in
         print "\tcmpq\t$%i, %%r10" exc;
         print "\tjne\t%s" label_handler_end;
         let env'' = compile_decl false env' (CDECL(fooloc,name)) in
         print "\tpushq\t%%rax";
         compile_loccode env'' handler_loccode;
         print "\tpopq\t%%rax"; (* foo *)
         print "\tjmp\t%s" label_finally;
         print "\t  %s:" label_handler_end;
       )
       handlers;
      (* For uncaught exceptions: memorize the exception to be propagated: *)
      print "\tmovq\t%%r10, 0(%%rsp)";
      (* “Finally” clause. *)
      print "  %s:" label_finally;
      print "\tpushq\t%%rax";
      let env' = (off-2,objs) in
      compile_loccode env' finally_loccode;
      (* Restore the stack. *)
      print "\tpopq\t%%rax";
      print "\tpopq\t%%r10";
      (* Rethrow an exception if uncaught. *)
      print "\tcmpq\t$0, %%r10";
      print "\tje\t%s" label_end;
      print "\tmovq\t%%rbp, %%rsp";
      print "\tjmp\t*(%%rsp)";
      print "  %s:" label_end

and compile_locexpr env (loc,expr) =
  compile_expr env loc expr

(* Compile an expression. *)
and compile_expr (off,objs as env) loc = function
  (* string literal: *)
  | STRING value ->
      let label = Genlab.genlab "string_litteral" in
      (* We need to store the string somewhere in a data section. *)
      pending "";
      pending ".section .data";
      pending "\t.local\t%s" label;
      pending "\t.type\t%s, @object" label;
      pending "\t.align\t4";
      pending "\t.size\t%s, %u" label (String.length value + 1);
      pending "  %s:" label;
      pending "\t.string \"%s\"" (String.escaped value);
      print "\tmovq\t$%s, %%rax" label
  (* integer constant: *)
  | CST value ->
      print "\tmovq\t$%i, %%rax" value
  (* variable name: *)
  | VAR name ->
      print "\tmovq\t%s, %%rax" (get_variable env loc name)
  (* assignation to a variable: *)
  | SET_VAR (name,locexpr) ->
      compile_locexpr env locexpr;
      print "\tmovq\t%%rax, %s" (get_variable env loc name)
  (* assignation to a subscripted variable: *)
  | SET_ARRAY (name,loci,locexpr) ->
      (* A generic solution: *)
      (*print "\tpushq\t%%rbx";
        compile_locexpr (off-1,objs) locexpr;
        print "\tmovq\t%%rax, %%rbx";
        print "\tmovq\t%%rbx, %s"
         (compile_lvalue (off-1,objs) loc (OP2(S_INDEX,(loc,VAR name),loci)));
        print "\tmovq\t%%rbx, %%rax";
        print "\tpopq\t%%rbx"*)
      (* We can optimize this a bit by knowing that the subscripted expression
         is a variable name: *)
      print "\tpushq\t%%rbx";
      compile_locexpr (off-1,objs) locexpr;
      print "\tmovq\t%%rax, %%rbx";
      compile_locexpr (off-1,objs) loci;
      print "\tmovq\t%s, %%r10" (get_variable (off-1,objs) loc name);
      print "\tmovq\t%%rbx, (%%r10,%%rax,%u)" Config.word;
      print "\tmovq\t%%rbx, %%rax";
      print "\tpopq\t%%rbx"
  (* function call: *)
  | CALL (name,locexprs) ->
      let nargs = List.length locexprs in
      let name = get_function env loc name nargs in
      let (_, first_regs) =
       List.split (Util.list_zip_truncate locexprs Config.call_registers) in
      let p = nargs - (List.length first_regs) in
      (* We must ensure that the stack pointer is 16-byte-aligned before
         calling a function. *)
      let padding = (p-off) mod 2 in
      allocate padding;
      let off' = off - padding in
      (* Compute each argument from right to left and push them to the stack. *)
      List.iteri
       (fun i arg ->
         compile_locexpr (off'-i,objs) arg;
         print "\tpushq\t%%rax"
       )
       (List.rev locexprs);
      (* Put the first six to registers *)
      List.iter (fun reg -> print "\tpopq\t%%%s" reg) first_regs;
      (* Call the function (some standard functions need %rax to be 0). *)
      print "\tmovq\t$0, %%rax";
      print "\tcallq\t%s" name;
      (** patch begin /************************************)
      (* Some standard functions incorrectly behave in a 32-bit fashion; this
         hack fix some issues. *)
      if not (object_exists objs name) && (name <> "malloc") then begin
        if Config.debug then
          print "\t# hack for stdlib functions with a 32-bit behaviour:";
        print "\tmovslq\t%%eax, %%rax"
      end;
      (**************************************/ patch end **)
      (* Restore the stack. *)
      free (padding + p)
  (* prefix or postfix ++/-- (that is, unary operator modifying a lvalue): *)
  | OP1 (monop,locexpr) when monop <> M_MINUS && monop <> M_NOT ->
      let lvalue = compile_loclvalue env locexpr in
      begin match monop with
        | M_POST_INC -> print "\tmovq\t%s, %%rax" lvalue;
                        print "\tincq\t%s"        lvalue
        | M_POST_DEC -> print "\tmovq\t%s, %%rax" lvalue;
                        print "\tdecq\t%s"        lvalue
        | M_PRE_INC  -> print "\tincq\t%s"        lvalue;
                        print "\tmovq\t%s, %%rax" lvalue
        | M_PRE_DEC  -> print "\tdecq\t%s"        lvalue;
                        print "\tmovq\t%s, %%rax" lvalue
        | _ -> raise Wont_happen
      end
  (* other unary operator: *)
  | OP1 (monop,locexpr) ->
      compile_locexpr env locexpr;
      begin match monop with
        | M_MINUS    -> print "\tnegq\t%%rax"
        | M_NOT      -> print "\tnotq\t%%rax"
        | _ -> raise Wont_happen
      end
  (* array subscripting (‘a[i]’): *)
  | OP2 (S_INDEX,locexpr1,locexpr2) as expr ->
      (* The generic solution: *)
      (*print "\tmovq\t%s, %%rax" (compile_lvalue env loc expr)*)
      (* Again, a few optimizations: *)
      print "\tpushq\t%%rbx";
      compile_locexpr (off-1,objs) locexpr2;
      print "\tmovq\t%%rax, %%rbx";
      compile_locexpr (off-1,objs) locexpr1;
      print "\tmovq\t(%%rax,%%rbx,%u), %%rax" Config.word;
      print "\tpopq\t%%rbx"
  (* other binary operator: *)
  | OP2 (binop,locexpr1,locexpr2) ->
      print "\tpushq\t%%rbx";
      compile_locexpr (off-1,objs) locexpr2;
      print "\tmovq\t%%rax, %%rbx";
      compile_locexpr (off-1,objs) locexpr1;
      begin match binop with
        | S_MUL   -> print "\timulq\t%%rbx, %%rax"
        | S_DIV   -> print "\tcqto";
                     print "\tidivq\t%%rbx"
        | S_MOD   -> print "\tcqto";
                     print "\tidivq\t%%rbx";
                     print "\tmovq\t%%rdx, %%rax"
        | S_ADD   -> print "\taddq\t%%rbx, %%rax"
        | S_SUB   -> print "\tsubq\t%%rbx, %%rax"
        | S_INDEX -> raise Wont_happen
      end;
      print "\tpopq\t%%rbx"
  (* comparison operator: *)
  | CMP (cmpop,locexpr1,locexpr2) ->
      let label_true = Genlab.genlab "cmp_true" in
      let label_end  = Genlab.genlab "cmp_end" in
      let jump_mnemonic = begin match cmpop with
       | C_LT -> "jl"
       | C_LE -> "jle"
       | C_EQ -> "je"
      end in
      print "\tpushq\t%%rbx";
      compile_locexpr (off-1,objs) locexpr2;
      print "\tmovq\t%%rax, %%rbx";
      compile_locexpr (off-1,objs) locexpr1;
      print "\tcmpq\t%%rbx, %%rax";
      print "\t%s\t%s" jump_mnemonic label_true;
      print "\tmovq\t$0, %%rax";
      print "\tjmp\t%s" label_end;
      print "  %s:" label_true;
      print "\tmovq\t$1, %%rax";
      print "  %s:" label_end;
      print "\tpopq\t%%rbx"
  (* ternary operator (‘a?b:c’): *)
  | EIF (locexpr1,locexpr2,locexpr3) ->
      let label_else = Genlab.genlab "ternary_else" in
      let label_end  = Genlab.genlab "ternary_end" in
      compile_locexpr env locexpr1;
      print "\tcmpq\t$0, %%rax";
      print "\tje\t%s" label_else;
      compile_locexpr env locexpr2;
      print "\tjmp\t%s" label_end;
      print "  %s:" label_else;
      compile_locexpr env locexpr3;
      print "  %s:" label_end
  (* sequence operator (‘a,b’): *)
  | ESEQ locexprs ->
      List.iter (compile_locexpr env) locexprs

and compile_loclvalue env (loc,expr) =
  compile_lvalue env loc expr

(* Compile a lvalue, that is, return a bit of assembly code with which the
   lvalue can be refered to, possibly after having produced some assembly
   instructions for preliminary computation (must respect the same conventions
   as the evaluation of expressions, and the returned value might depend on
   the contents of %r10). *)
and compile_lvalue (off,objs as env) loc = function
  | VAR name ->
      get_variable env loc name
  | OP2 (S_INDEX,locexpr1,locexpr2) ->
      print "\tpushq\t%%rbx";
      compile_locexpr (off-1,objs) locexpr2;
      print "\tmovq\t%%rax, %%rbx";
      compile_locexpr (off-1,objs) locexpr1;
      print "\tleaq\t(%%rax,%%rbx,%u), %%r10" Config.word;
      print "\tpopq\t%%rbx";
      "(%r10)"
  | _ ->
      error (Some loc) "this is not a lvalue";
      ""

(* Return a bit of assembly code designating the variable whose name is given. *)
and get_variable (off,objs as env) loc name =
  try
    let obj = find_object objs name in
    if obj.otype <> Var then
      error (Some loc) "object ‘%s’ is not a variable" name;
    begin match obj.storage with
      | Static        -> Printf.sprintf "%s" name
      | Stack obj_off -> Printf.sprintf "%i(%%rsp)" (Config.word * (obj_off - off))
      | Register reg  -> Printf.sprintf "%%%s" reg
    end
  with Not_found ->
    warning (Some loc) "unknown variable ‘%s’ (declared elsewhere?)" name;
    name

(* Same thing for functions. Actually, as only static functions are supported,
   just do some checkings (which are not strictly necessary for compiling). *)
and get_function (off,objs as env) loc name nargs =
  try
    let obj = find_object objs name in
    begin match obj.otype with
      | Var ->
          error (Some loc) "object ‘%s’ is not a function" name
      | Func nparams -> if nparams <> nargs then
          warning (Some loc) "calling ‘%s’ with %u argument(s) but expecting %u" name nargs nparams
    end;
    if obj.storage <> Static then
      raise (Not_implemented "non-global functions");
    name
  with Not_found ->
    warning (Some loc) "unknown function ‘%s’ (declared elsewhere?)" name;
    name

(* Allocate the given number of words in the stack. *)
and allocate words =
  if words <> 0 then
    print "\tsubq\t$%u, %%rsp" (Config.word * words)

(* Free the given number of words in the stack. *)
and free words =
  if words <> 0 then
    print "\taddq\t$%u, %%rsp" (Config.word * words)
